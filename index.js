module.exports = {
  'extends': ['tslint-microsoft-contrib', 'tslint-eslint-rules'],
  'rules': {
    'no-backbone-get-set-outside-model': false,
    'await-promise': [true, 'Bluebird'],
    'no-parameter-properties': false,
    'no-import-side-effect': false,
    'semicolon': [true, 'never'],
    'missing-jsdoc': false,
    'no-relative-imports': false,
    'strict-boolean-expressions': false,
    'no-default-export': false,

    // https://github.com/palantir/tslint-react/issues/120
    'variable-name': [true, 'ban-keywords', 'check-format', 'allow-pascal-case'],
    // https://github.com/palantir/tslint/issues/3621
    'no-unsafe-any': false,
    // https://github.com/palantir/tslint/issues/3364
    'no-implicit-dependencies': false,
    // https://github.com/palantir/tslint/issues/3483
    'no-submodule-imports': false,
    'ordered-imports': [true, {
      'grouped-imports': true,
      'import-sources-order': 'any',
      'named-imports-order': 'any'
    }],

    // next two for uniformity in ts and tsx, only allow 'as' type cast
    'no-angle-bracket-type-assertion': true,
    'prefer-type-cast': false,

    'import-name': false, // import Layout from 'components/shared/layout'

    'whitespace': [
      true,
      'check-branch',
      'check-decl',
      'check-operator',
      'check-separator',
      'check-type',
      'check-module'
    ],

    'ter-indent': [true, 2],

    'completed-docs': false,
    'object-curly-spacing': [
      true,
      'always'
    ],

    'no-reserved-keywords': false,
    'underscore-consistent-invocation': false,
    'no-increment-decrement': false
  },
  'linterOptions': {
    'exclude': [
      './node_modules'
    ]
  }
}
